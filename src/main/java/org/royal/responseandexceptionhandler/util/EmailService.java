package org.royal.responseandexceptionhandler.util;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.royal.responseandexceptionhandler.Entities.UserEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Component
@EnableAsync
public class EmailService {
    private final JavaMailSender mailSender;
    @Value("${royal.notification.email}")
    private String from;

    public EmailService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void sendSimpleMessage(String to, String subject, String text) {

        // Creating a mime message
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper;

        try {
            // Setting multipart as true for attachments to be send
            mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            mimeMessageHelper.setFrom("mistryriddhi10@gmail.com");
            mimeMessageHelper.setTo(to);
            mimeMessageHelper.setText(text, true);
            mimeMessageHelper.setSubject(subject);
            mailSender.send(mimeMessage);
        } catch (Exception e) {
            System.out.println("");
        }
    }

    @Async
    public void sendEmail(String to, String subject, UserEntity user, String tempName) throws MessagingException, UnsupportedEncodingException {

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        var personal = "Royal Notification System";
        Map<String,String> data = new HashMap<String,String>();
        data.put("${user}",user.getFirstName());
        data.put("${username}",user.getFirstName());
        data.put("${email}",user.getEmail());
        var temp = EmailService.readDataFromFile(tempName);
        for(Map.Entry<String,String> entry : data.entrySet()) {
            temp = temp.replace(entry.getKey(), entry.getValue());
        }
        //data.entrySet().forEach((d, v)-> temp = temp.replace(d,v));
        //temp = temp.replace("${user}", user.getFirstName());
        helper.setTo(to);
        helper.setFrom(from, personal);
        helper.setSubject(subject);
        helper.setText(temp, true);

        //FileResource fileResource = new FileResource(new File())

        // helper.addAttachment("xyz",);
        mailSender.send(message);
    }


    private static String readDataFromFile(String tempName) {
        try {
            return new String(new FileInputStream("src/main/resources/templates/" + tempName).readAllBytes(), StandardCharsets.UTF_8);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
