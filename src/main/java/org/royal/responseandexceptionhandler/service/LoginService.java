package org.royal.responseandexceptionhandler.service;

import org.royal.responseandexceptionhandler.dtos.LoginRequestDto;
import org.royal.responseandexceptionhandler.util.APIResponse;

public interface LoginService {

    APIResponse login(LoginRequestDto loginRequestDto);
}
