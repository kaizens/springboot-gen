package org.royal.responseandexceptionhandler.service;

import org.royal.responseandexceptionhandler.dtos.UserDto;
import org.royal.responseandexceptionhandler.util.APIResponse;

import java.util.UUID;

public interface UserService {
    APIResponse save(UserDto userDto);

    APIResponse getById(UUID userId);
}
