package org.royal.responseandexceptionhandler.service.impl;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.royal.responseandexceptionhandler.Entities.UserEntity;
import org.royal.responseandexceptionhandler.dtos.UserDto;
import org.royal.responseandexceptionhandler.exceptions.handler.UserNotFoundException;
import org.royal.responseandexceptionhandler.repository.UserRepository;
import org.royal.responseandexceptionhandler.service.UserService;
import org.royal.responseandexceptionhandler.util.APIResponse;
import org.royal.responseandexceptionhandler.util.EmailService;
import org.royal.responseandexceptionhandler.util.RandomPasswordGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class UserServiceImpl implements UserService {

    private final ModelMapper modelMapper;
    private final UserRepository userRepository;
    @Autowired
    private EmailService emailService;

    @Override
    public APIResponse save(UserDto userDto) {
        UserEntity user = modelMapper.map(userDto, UserEntity.class);
        String password = RandomPasswordGenerator.generateRandomPassword();
        userRepository.save(user);
        try {
            emailService.sendEmail(userDto.getEmail(), "Registration Email", user, "Template.html");
            user.setPassword(password);
        } catch (Exception e) {
            e.printStackTrace();
            throw new UserNotFoundException("Something went wrong with email");
        }
        return new APIResponse("User Created Successfully");
    }

    @Override
    public APIResponse getById(UUID userId) {
        Optional<UserEntity> entity = userRepository.findById(userId);
        return new APIResponse(HttpStatus.OK.value(), entity.get());
    }
}
