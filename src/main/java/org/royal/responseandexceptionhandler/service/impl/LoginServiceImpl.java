package org.royal.responseandexceptionhandler.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.royal.responseandexceptionhandler.dtos.LoginRequestDto;
import org.royal.responseandexceptionhandler.exceptions.handler.UserNotFoundException;
import org.royal.responseandexceptionhandler.service.LoginService;
import org.royal.responseandexceptionhandler.util.APIResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService {

    private static final String username = "royal";
    private static final String password = "royal";

    @Override
    public APIResponse<?> login(LoginRequestDto loginRequestDto) {
        log.info("login request");
        log.debug("inside logic {}", loginRequestDto);
        if (username.equals(loginRequestDto.getUsername())){
            int x = 10/0;
            if (password.equals(loginRequestDto.getPassword())) {
                return new APIResponse<>(HttpStatus.OK.value(), "login Successfully");
            }
            return new APIResponse<>("password not matched", HttpStatus.BAD_REQUEST.value());
        }

        throw new UserNotFoundException("username not found");
        //return new APIResponse<>("username not found", HttpStatus.BAD_REQUEST.value());
    }
}
