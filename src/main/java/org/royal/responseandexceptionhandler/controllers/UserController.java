package org.royal.responseandexceptionhandler.controllers;

import lombok.RequiredArgsConstructor;
import org.royal.responseandexceptionhandler.dtos.UserDto;
import org.royal.responseandexceptionhandler.service.UserService;
import org.royal.responseandexceptionhandler.util.APIResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping
    public ResponseEntity<APIResponse> save(@RequestBody UserDto userDto){
        return new ResponseEntity<>(userService.save(userDto), HttpStatus.CREATED);
    }
    @GetMapping("/{userId}")
    public ResponseEntity<APIResponse> save(@PathVariable("userId") UUID userId){
        return new ResponseEntity<>(userService.getById(userId), HttpStatus.OK);
    }
}
