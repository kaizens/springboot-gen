package org.royal.responseandexceptionhandler.controllers;

import org.royal.responseandexceptionhandler.dtos.LoginRequestDto;
import org.royal.responseandexceptionhandler.service.LoginService;
import org.royal.responseandexceptionhandler.util.APIResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping
    public ResponseEntity<APIResponse> login(@RequestBody LoginRequestDto loginRequestDto) {
        return new ResponseEntity<>(loginService.login(loginRequestDto), HttpStatus.OK);
    }

    @GetMapping("{username}")
    public void find(@PathVariable("username") String username) {
        int x = 10/0;
        System.out.println("calleedddd");
    }
}
