package org.royal.responseandexceptionhandler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ResponseAndExceptionHandlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResponseAndExceptionHandlerApplication.class, args);
	}

}
