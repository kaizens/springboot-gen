package org.royal.responseandexceptionhandler.Scheduler;

import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import org.royal.responseandexceptionhandler.repository.UserRepository;
import org.royal.responseandexceptionhandler.util.EmailService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class Test {

    private final UserRepository repository;
    private final EmailService emailService;

    @Scheduled(fixedRate = 10000)
    private void displayData() {
        repository.findAll().parallelStream()
                .forEach(data -> {
                    System.out.println(data);
                    if (Objects.equals(data.getBirthDate(), LocalDate.now())) {
                        try {
                            emailService.sendEmail(data.getEmail(), "CroneTesting", data, "birthday.html");
                        } catch (MessagingException | UnsupportedEncodingException e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
        System.out.println("My crone executed at: " + LocalTime.now());
    }
}
