package org.royal.responseandexceptionhandler.Scheduler;

import jakarta.mail.MessagingException;
import org.royal.responseandexceptionhandler.repository.UserRepository;
import org.royal.responseandexceptionhandler.util.EmailService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.util.Objects;

@Async
@Component
public class BirthdayNotification {

    private final UserRepository userRepository;
    private final EmailService emailService;

    public BirthdayNotification(UserRepository userRepository, EmailService emailService) {
        this.userRepository = userRepository;
        this.emailService = emailService;
    }

    //@Scheduled(cron = "1 * * * * *")
    private void sendNotification() {
        System.out.println("crone executed at " + LocalDate.now());
        userRepository.findAll().stream().parallel().forEach(data -> {
            System.out.println(data);
            if(Objects.equals(data.getBirthDate(), LocalDate.now())){
                try {
                    System.out.println("mail sended at " + LocalDate.now());
                    emailService.sendEmail(data.getEmail(),"Birthday Wish",data,"birthday.html");
                } catch (MessagingException | UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
